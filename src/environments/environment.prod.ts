export const environment = {
  production: true,
  endPoint:'http://localhost:8080/',
  getHackerNews:'api/reign/getInfo',
  removeHackerNews:'api/reign/removeInfo'
};
