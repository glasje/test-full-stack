import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';


//components
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';


//shared
import { SharedModule } from './shared/shared.module';
import { ModulesModule } from './modules/modules.module';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    ModulesModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
