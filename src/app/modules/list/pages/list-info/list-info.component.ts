import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { InfoService } from 'src/app/shared/info/info.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../../components/modal/modal.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-info',
  templateUrl: './list-info.component.html',
  styleUrls: ['./list-info.component.scss']
})
export class ListInfoComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  constructor(private infoServices : InfoService,public dialog: MatDialog,private snackBar: MatSnackBar) { }
  lstHackerNews : any;
  async ngOnInit() {

    let datos = await this.infoServices.getHackerNews();
  
    this.prepararDatos(datos);
  
  }

  prepararDatos(datos){
    let lstNackerNew =[];
    datos.map(hackerNew=>{
      if(!hackerNew.story_title && hackerNew.title){
        hackerNew.story_title=hackerNew.title;
        lstNackerNew.push(hackerNew);
     
      }
    })
    lstNackerNew.sort((a,b)=>a.created_at>b.created_at?-1:null);
    //datos = datos.filter(hackerNew =>(hackerNew.story_title && hackerNew.title ))
    this.lstHackerNews = new MatTableDataSource<any>(lstNackerNew);
    this.lstHackerNews.paginator = this.paginator;
   
  }
  eliminarElemento(elemento){
  
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px',
      data: {elemento}
    });

    dialogRef.afterClosed().subscribe(async result => {
 
      if(result){
         let respuesta = await this.infoServices.deleteHackNews(result);

      if(respuesta>0){
        this.openSnackBar();
        let datos = await this.infoServices.getHackerNews();
        this.prepararDatos(datos);
      }
      }
     
    });
  }
    
   openSnackBar() {
    this.snackBar.open('se elimino correctamente','Hackers News', {
      duration: 2000,
    });
  }
}
