import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulesRoutingModule } from './modules-routing.module';
import { ListInfoComponent } from './list/pages/list-info/list-info.component';
import { SharedModule } from '../shared/shared.module';
import { ModalComponent } from './list/components/modal/modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  entryComponents:[ModalComponent],
  declarations: [
    ListInfoComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
  ],
  exports:[
    ListInfoComponent
  ]
})
export class ModulesModule { }
