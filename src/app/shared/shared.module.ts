import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoService } from './info/info.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//material
import { MatSliderModule } from '@angular/material/slider';
import {CdkTableModule} from '@angular/cdk/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSliderModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    BrowserAnimationsModule
  ],
  providers:[
    InfoService
  ],
  exports:[
    MatSliderModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule
  ]
})
export class SharedModule { }
