import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(private httpClient : HttpClient) { }

  async getHackerNews(){
    let consulta =null;
    const url = `${environment.endPoint}${environment.getHackerNews}`;
    consulta= await this.httpClient.get(url).toPromise();
    if(consulta.error){
      return [];
    }
    return consulta.data;

  }

  async deleteHackNews(data){
    let consulta =null;
    const url = `${environment.endPoint}${environment.removeHackerNews}?id=${data.objectID}&create=${data.created_at}`;
    consulta= await this.httpClient.get(url).toPromise();
    if(consulta.error){
      return [];
    }
    return consulta.data;

  }
}
